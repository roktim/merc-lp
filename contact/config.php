<?php
/**
* Adding configuration like sending email or SMTP config
* @author Utpal Biswas <utpal.uoda@gmail.com>
*/

//SMTP config
$smtpConfig = array(
  'host' => 'smtp.gmail.com',
  'username' => '',
  'password' => '',
  'mailto' => 'info@yu-m-k.com',
  'mailtoContact' => 'info@yu-m-k.com',
  'redirecturl' => 'success.html',

);

?>
