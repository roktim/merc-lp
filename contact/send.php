<?php
/**
* Sending mail using php mailer
* @author Utpal Biswas<utpal.uoda@gmail.com>
*/
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';
require 'config.php';



session_start();
// define variables and set to empty values
$mailSys = $smtpConfig['mailto'];
$mailTo = $smtpConfig['mailtoContact'];
$redirectUrl = $smtpConfig['redirecturl'];
$mailSuccess = false;

 $querytype = $email = $inquiry = "";
if ($_SERVER['REQUEST_METHOD'] == "POST") {

  $querytype = $_POST["querytype"];
  $email = $_POST["email"];
  $inquiry = $_POST["inquiry"];
}

//create mail body
$body  = "以下の内容にてお問い合わせがありました。\n<br><br>";
$body .= "----------------------------------------------------------------\n<br>";
$body .= "お問い合わせ内容: {$querytype}\n\n\r<br>";
$body .= "メールアドレス: {$email} \n\r<br>";
$body .= "お問い合わせ内容: \n\r<br><br>";

$body .= "{$inquiry} \n\r<br><br>";

$body .= "----------------------------------------------------------------\n<br><br>";


$mail = new PHPMailer(true);
try {
    //Server settings
    $mail->SMTPDebug = 0;                        // Enable verbose debug output
    $mail->CharSet = "UTF-8";
    // $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = $smtpConfig['host'];                       // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    // $mail->Username = $smtpConfig['username'];           // SMTP username
    // $mail->Password = $smtpConfig['password'];                   // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    //Recipients
    $mail->setFrom($mailSys, 'merc Education お問い合わせ');
    $mail->addAddress($mailTo);               // Add a recipient Name is optional
    $mail->addReplyTo($mailSys, 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'merc Educationへのお問い合わせがありました。';
    $mail->Body    = $body;
    $mail->AltBody = $body;

    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
      ob_start();
      header('Location: '.$redirectUrl);
      $mailSuccess = true;
      echo 1;
      ob_end_flush();
      die();
    }

} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

session_unset();
?>
