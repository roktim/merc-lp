<?php
 $querytype = $email = $inquiry = "";
if ($_SERVER['REQUEST_METHOD'] == "POST") {

  $querytype = $_POST["querytype"];
  $email = $_POST["email"];
  $inquiry = $_POST["inquiry"];
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0" />
  <link rel="shortcut icon" type="images/favicon.ico" href="../images/favicon.ico">
  <title>Merc LP</title>
  <meta name="keywords" content="" />
  <meta name="description" content="120文字以内" />
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">
  <link rel="stylesheet" href="../css/inner-page.css">
  <link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico">
</head>

<body>
  <div id="left-menu" class="left-meuu">
  </div>
  <div class="sp-top-menu clearfix sp-nav-btn">
    <div class="sp-menu-rap clearfix">
      <div class="logo"><a href="../"><img src="../images/sp/header_logo@2x.png" alt="logo"></a></div>
      <div class="sp-nav sp-nav-open clearfix">
        <div class="navline"></div>
        <div class="navline"></div>
      </div>
    </div>
  </div>
  <div class="sp-navigation" id="sp-navigation"></div>
  <div class="main-container">
    <div class="inner-container clearfix">
      <div class="contact-heading contact-heading-preview clearfix">
        <h2>お問い合わせ内容の確認</h2>
      </div>
      <div class="form-rap form-rap-preview clearfix">
        <div class="main-form clearfix">
          <form id="contactForm" class="contact-frm" action="send.php" method="post" name="contacts">
            <div class="input-group">
              <div class="input-lbl">
                <p>お問い合わせ内容</p>
              </div>
              <div class="action-fild inquery-type">
                <div class="field-value clearfix">
                  <p><?php echo $querytype; ?></p>
                  <input type="hidden" name="querytype" value="<?php echo $querytype; ?>">
                </div>
              </div>
            </div>

            <div class="input-group mail-group">
              <div class="input-lbl">
                <p>メールアドレス</p>
              </div>
              <div class="action-fild">
                <div class="field-value clearfix">
                  <p><?php echo $email; ?></p>
                  <input type="hidden" name="email" value="<?php echo $email; ?>">
                </div>
              </div>
            </div>
            <div class="input-group inquery-group inquery-group-preview">
              <div class="input-lbl">
                <p>お問い合わせ内容</p>
              </div>
              <div class="action-fild">
                <div class="field-value clearfix">
                  <p>
                    <?php echo $inquiry; ?>
                  </p>
                  <input type="hidden" name="inquiry" value="<?php echo $inquiry; ?>">
                </div>
              </div>
            </div>
            <div class="submit-btn-rap submit-btn-rap-preview">
              <button class="btn-submit animi-btn" type="submit" name="submit">
                送信する
                <img src="../images/np_right-arrow@2x.png" alt="submit">
              </button>
              <div class="back-to-edit clearfix">
                <a href="#" onclick="window.history.go(-1); return false;">
                  <img class="pc" src="../images/contact2_btn@2x.png" alt="back">
                  <img class="sp" src="../images/contact2btn@2x.png" alt="back">
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="footer clearfix">
      <p>©︎copyright merc Education ALL RIGHTS RESERVED.</p>
    </div>
  </div>
  <div class="back-to-up">
    <img src="../images/top@2x.png" alt="back top">
  </div>


  <script>
    window.jQuery || document.write('<script src="../js/jquery-1.12.0.min.js"><\/script>')
  </script>
  <script src="../js/script.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#left-menu').load("../left-menu.html");
      $('#sp-navigation').load("../sp-navigation.html");
      $('.animi-btn').mouseover(function () {
        $(this).find('img').attr('src', '../images/np_right-arrow_white@2x.png')
      });
      $('.animi-btn').mouseleave(function () {
        $(this).find('img').attr('src', '../images/np_right-arrow@2x.png')
      });
    });
  </script>
</body>

</html>