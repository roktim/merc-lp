$(document).ready(function () {
	var isMobile = window.matchMedia("only screen and (max-width: 1024px)");

	$(".back-to-up").hide();
if (!isMobile.matches) {
  	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('.back-to-up').fadeIn();
			} else {
				$('.back-to-up').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('.back-to-up img').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
  }
  
  $(".mobile-nav .nav").click(function () { 
    $(".sp-main-nav").show();
			$("body").addClass("no-scroll");
  });
  		$(document).on('click', '.sp-nav-close',function () {
			$(".sp-main-nav").hide();
			$("body").removeClass("no-scroll");
		});


});


var scrollAmout = 200;

$(window).on("scroll", function() {

  scr = $(window).scrollTop();
  missionLink = $("#our-mission").offset().top;
  visionLink = $("#our-vision").offset().top;
  programLink = $("#our-program").offset().top;
  trailLink = $("#trial-class").offset().top;
  ourpartLink = $("#our-part").offset().top;




  if (scr < missionLink - scrollAmout) {
    $(".right-menu a").removeClass("active");
  }

  if (scr > missionLink - scrollAmout) {
    $(".right-menu a").removeClass("active");
    $(".right-menu a").eq(0).addClass("active");
  }

  if (scr > visionLink - scrollAmout) {
    $(".right-menu a").removeClass("active");
    $(".right-menu a").eq(1).addClass("active");
  }

  if (scr > programLink - scrollAmout) {
    $(".right-menu a").removeClass("active");
    $(".right-menu a").eq(2).addClass("active");
  }

  if (scr > trailLink - scrollAmout) {
    $(".right-menu a").removeClass("active");
    $(".right-menu a").eq(3).addClass("active");
  }

  if (scr > ourpartLink - scrollAmout) {
    $(".right-menu a").removeClass("active");
    $(".right-menu a").eq(4).addClass("active");
	}

});
