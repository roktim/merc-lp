$(document).ready(function () {
/*
  Author:Rfiat
  description: pop up on  click*/
  $(".ily-view").on("click", function (e) {
    e.preventDefault();
    $('#ily-popup').show();
    $('body').css('overflow', 'hidden');
  });
  $("#ily-popup").on("click", function (e) {
    e.preventDefault();
    $(this).hide();
    $('body').css('overflow', 'auto');
  });
  $(".close-popup").on("click", function (e) {
    e.preventDefault();
    $('#ily-popup').hide();
    $('body').css('overflow', 'auto');
  });
});

$(window).on('load resize', function () {
  var screenWidth = $(window).width();

  var trielDiv = $('#trial-class .right-section .content .class-list .class-detail').height();
  var arrowDiv = $(
    'body .container #trial-class .right-section .content .class-list .class-detail .inner-content .arrow-part .btn-detail'
  ).height();
  $('#trial-class .right-section .content .class-list .class-detail .inner-content .arrow-part .btn-detail')
    .css({
      'margin-top': parseInt((trielDiv / 2) - arrowDiv) + 2 + 'px'
    });
  $('#trial-class .right-section .content .class-list .class-detail .inner-content .arrow-part .btn-detail-top')
    .css({
      'margin-top': parseInt((trielDiv / 2) - arrowDiv) + 7 + 'px'
    });

  // header middle title
  if (screenWidth <= 1280 && screenWidth > 1024) {
    $('#banner-section .top-section .right-section .middle-part .box-title p').css({
      'font-size': '33px'
    });
    // $('#banner-section .top-section .left-section .menu-section ul li').css({
    //   'padding-left': '18%'
    // });
  } else if (screenWidth > 1280) {
    $('#banner-section .top-section .right-section .middle-part .box-title p').css({
      'font-size': '40px'
    });
    // $('#banner-section .top-section .left-section .menu-section ul li').css({
    //   'padding-left': '30%'
    // });
  }
  if (screenWidth <= 767 && screenWidth > 376) {
    $('#banner-section .middle-part .box-title p').css({
      'font-size': '1.1em',
      'left': '0',
    });
  } else if (screenWidth == 375) {
    $('#banner-section .middle-part .box-title p').css({
      'font-size': '1.3em',
      'left': '-11.7%',
    });
  } else if (screenWidth < 375) {
    $('#banner-section .middle-part .box-title p').css({
      'font-size': '1.1em',
      'left': '-11.7%',
    });
  }

  $('.btn-center a,.arrow-part .btn-detail a,.bottom-content .btn-details a').mouseover(function () {
    $(this).find('img').attr('src', 'images/np_right-arrow_white@2x.png')
  });
  $('.btn-center a,.arrow-part .btn-detail a,.bottom-content .btn-details a').mouseleave(function () {
    $(this).find('img').attr('src', 'images/np_right-arrow@2x.png')
  });
});