/*
 * Form validation
 */
var frmvalidator = new Validator("contactForm");
frmvalidator.EnableOnPageErrorDisplay();
frmvalidator.EnableMsgsTogether();
frmvalidator.addValidation("email", "req", "メールアドレスを入力してください");
frmvalidator.addValidation("email", "email", "メールアドレスを正しく入力してください");
frmvalidator.addValidation("inquiry", "req", "お問い合わせ内容を入力してください");


$("#contactSubmit").click(function() {
  submitform();
});
var message = '';

function submitform() {
  setTimeout(function() {
    var message = $(".valMsg").text();
    if (message == '') {
      document.getElementById("contactForm").submit();      
    }
  }, 100);
}
