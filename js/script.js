$(document).ready(function () {
	var isMobile = window.matchMedia("only screen and (max-width: 1024px)");

	$(".back-to-up").hide();
	if (!isMobile.matches) {
	  	$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
					$('.back-to-up').fadeIn();
				} else {
					$('.back-to-up').fadeOut();
				}
			});

			// scroll body to 0px on click
			$('.back-to-up img').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});
	}
// SP MENU SHOW HIDE
		$(document).on("click", ".sp-nav-open", function () {
			$(".sp-main-nav").show();
			$("body").addClass("no-scroll");
		});
		$(document).on('click', '.sp-nav-close',function () {
			$(".sp-main-nav").hide();
			$("body").removeClass("no-scroll");
		});
	
	if (isMobile.matches) {

		adjustMenuHeight();
		$(window).resize(function () {
			adjustMenuHeight();
		});
		$(window).resize(function () {
			if ($(window).width() > 1024) {
				$("body").css('padding-top', '0');
			}
		});
		
  }

  $('#left-menu').load("../left-menu.html",function(){
    let link=location.pathname;
    let secondLastIndex = link.lastIndexOf('/', link.lastIndexOf('/')-1)
    let fileLink=link.substring(parseInt(secondLastIndex+1));
    let file=fileLink.substring(0, fileLink.length - 1);
    $('.menu-section .mtop li').each(function(){
     
      if($(this).attr('data-link')==file){
        $(this).find('a').addClass('active');
        // $(this).addClass('d');
        // console.log($(this).find('a').html());
        
      }
    })
	});
	
	  
  $('.btn-center a,.arrow-part .btn-detail a,.bottom-content .btn-details a, .btn-details a').mouseover(function () {
		$(this).find('img').attr('src', '../images/np_right-arrow_white@2x.png')
  });
  $('.btn-center a,.arrow-part .btn-detail a,.bottom-content .btn-details a, .btn-details a').mouseleave(function () {
    $(this).find('img').attr('src', '../images/np_right-arrow@2x.png')
  });
});

function adjustMenuHeight() {
	var spMenuHeight = $(".sp-top-menu").height();		
	$("body").css('padding-top', spMenuHeight);
}